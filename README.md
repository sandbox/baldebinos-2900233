CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting

INTRODUCTION
------------

The Simple Form Disabler module allows you to disable some forms.

Basic features:

 * There are no other features.

REQUIREMENTS
------------

 * none

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.

CONFIGURATION
-------------

 * Configure as you would normally configure a contributed Drupal module.

TROUBLESHOOTING
---------------

 * Do it.
