<?php

/**
 * @file
 * Contains \Drupal\simple_form_disabler\Form\SimpleFormDisablerSettingsForm.
 */
namespace Drupal\simple_form_disabler\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Simple Form Disabler settings for this site.
 */
class SimpleFormDisablerSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_form_disabler_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Form constructor.
    $form = parent::buildForm($form, $form_state);

    // Default settings.
    $config = $this->config('simple_form_disabler.settings');

    // Search Form.

    $form['group_search'] = array(
      '#type' => 'fieldset',
      '#title' => t('Search'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );

    $form['group_search']['simple_form_disabler_search'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disable Search Form'),
      '#default_value' => $config->get('search'),
      '#description' => t('Disables Search Form in the Search Results Page.<br />
                           Standard Search Form in Search Block is not disabled.'),
    );

    $form['group_search']['simple_form_disabler_search_redirect'] = array(
      '#type' => 'checkbox',
      '#title' => t('Redirect Search to Not Found'),
      '#default_value' => $config->get('search_redirect'),
      '#description' => t('Prevents access to the Search Page by throwing a NotFoundHttpException.'),
    );

    // Login Form.

    $form['group_login'] = array(
      '#type' => 'fieldset',
      '#title' => t('Login'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );

    $form['group_login']['simple_form_disabler_login'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disable Login Form'),
      '#default_value' => $config->get('login'),
      '#description' => t('Disables Login Form in the User Page.<br />
                           When disabling this Form, it is strongly advised to set Excluded Addresses.<br />
                           Not setting at least one Excluded Address, might result in losing the capability<br />
                           of logging on, even for administrative purposes.'),
    );

    // Reset Password Form.
    $form['group_login']['simple_form_disabler_pass'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disable Reset Password Form'),
      '#default_value' => $config->get('pass'),
      '#description' => t('Disables Reset Password Form in the User Page.'),
    );

    $form['group_login']['simple_form_disabler_login_redirect'] = array(
      '#type' => 'checkbox',
      '#title' => t('Redirect Log In to Not Found'),
      '#default_value' => $config->get('login_redirect'),
      '#description' => t('Prevents access to the Log In Page by throwing a NotFoundHttpException.<br />
                           When redirecting the Log In Page, it is strongly advised to set Excluded Addresses.<br />
                           Not setting at least one Excluded Address, might result in losing the capability<br />
                           of logging on, even for administrative purposes.'),
    );

    // Exclude Addresses.
    $form['simple_form_disabler_exclude'] = array(
      '#type' => 'textfield',
      '#title' => t('Excluded Addresses'),
      '#default_value' => $config->get('exclude'),
      '#description' => t('Skip disabling Forms and redirecting Pages for the specified client address or addresses.<br />
                           When specifying multiple addresses, use a comma delimited list.'),
    );

    // Clear all caches.
    $form['simple_form_disabler_clear'] = array(
      '#type' => 'item',
      '#title' => t('Clear All Caches might be needed for changes to take effect.'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('simple_form_disabler.settings')
      ->set('search', $form_state->getValue('simple_form_disabler_search'))
      ->set('search_redirect', $form_state->getValue('simple_form_disabler_search_redirect'))
      ->set('login', $form_state->getValue('simple_form_disabler_login'))
      ->set('pass', $form_state->getValue('simple_form_disabler_pass'))
      ->set('login_redirect', $form_state->getValue('simple_form_disabler_login_redirect'))
      ->set('exclude', $form_state->getValue('simple_form_disabler_exclude'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'simple_form_disabler.settings',
    ];
  }

}
